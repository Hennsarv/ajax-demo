﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace WebApplication9
{
    public partial class Employee
    {
        public string FullName => FirstName + " " + LastName;
    }
}

namespace WebApplication9.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        [HttpPost]
        public JsonResult Employee(string employeeid)
        {
            using (northwindEntities ne = new northwindEntities())
            {
                int id = int.Parse(employeeid);
                var j = Json(ne.Employees
                   .Where(x => x.EmployeeID == id).ToList()
                   .Select(x => new { x.FullName, HireDate = x.HireDate?.ToShortDateString() })
                   .SingleOrDefault());
                return j;
            }
        }
    }
}